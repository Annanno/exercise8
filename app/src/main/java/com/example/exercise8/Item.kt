package com.example.exercise8

data class Item(val image: Int, val person: Person? = null ) {
    data class Person (val firstname: String, var lastname: String )
}