package com.example.exercise8

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.exercise8.databinding.FirstItemLayoutBinding
import com.example.exercise8.databinding.SecondItemLayoutBinding

typealias ClickItem = (item: Item, position: Int) -> Unit
typealias LongClickItem = (item: Item, position: Int) -> Unit

class Adapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        private const val FIRST_ITEM = 1
        private const val SECOND_ITEM = 0
    }

    private val items = mutableListOf<Item>()
    var callback: ClickItem? = null
    var longClick: LongClickItem? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == FIRST_ITEM) {
            FirstViewHolder(
                FirstItemLayoutBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
        } else {
            SecondViewHolder(
                SecondItemLayoutBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
        }

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is FirstViewHolder) {
            holder.onBind()
        } else if (holder is SecondViewHolder) {
            holder.onBind()
        }
    }

    override fun getItemViewType(position: Int) =
        if (items[position].person != null) SECOND_ITEM else FIRST_ITEM


    override fun getItemCount() = items.size

    inner class FirstViewHolder(private val binding: FirstItemLayoutBinding) :
        RecyclerView.ViewHolder(binding.root), View.OnClickListener, View.OnLongClickListener {
        private lateinit var model: Item
        fun onBind() {
            model = items[adapterPosition]
            binding.image.setImageResource(model.image)
            binding.image.setOnClickListener(this)
            binding.image.setOnLongClickListener(this)

        }

        override fun onClick(v: View?) {
            callback?.invoke(model, adapterPosition)
        }

        override fun onLongClick(v: View?): Boolean {
            longClick?.invoke(model, adapterPosition)
            delete(adapterPosition)
            return true
        }

    }

    inner class SecondViewHolder(private val binding: SecondItemLayoutBinding) :
        RecyclerView.ViewHolder(binding.root), View.OnClickListener, View.OnLongClickListener {
        private lateinit var model: Item
        fun onBind() {
            model = items[adapterPosition]
            binding.name.text = model.person?.firstname
            binding.lastName.text = model.person?.lastname
            binding.root.setOnClickListener(this)
            binding.addButton.setOnClickListener(this)
            binding.lastName.setOnClickListener(this)
            binding.root.setOnLongClickListener(this)


        }

        override fun onClick(v: View?) {
            callback?.invoke(model, adapterPosition)
            if (v == binding.addButton) {
                addItem(model)
            } else if (v == binding.lastName) {
                update(adapterPosition)
            }
        }

        override fun onLongClick(v: View?): Boolean {
            longClick?.invoke(model, adapterPosition)
            delete(adapterPosition)
            return true
        }

    }

    fun setData(mutableList: MutableList<Item>) {
        this.items.clear()
        this.items.addAll(mutableList)
        notifyDataSetChanged()
    }

    fun delete(position: Int) {
        this.items.removeAt(position)
        notifyItemRemoved(position)
    }

    fun update(position: Int) {
        items[position].person?.lastname = " "
        notifyItemChanged(position)
    }

    fun addItem(item: Item) {
        this.items.add(item)
        notifyItemInserted(items.size)

    }
}