package com.example.exercise8

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import com.example.exercise8.databinding.ActivityMainBinding

typealias mipmap = R.mipmap

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var adapter: Adapter
    private val items = mutableListOf<Item>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
    }

    private fun init() {
        setData()

        binding.recyclerView.layoutManager = GridLayoutManager(this, 3)
        adapter = Adapter()
        binding.recyclerView.adapter = adapter
        adapter.longClick = { item, position ->
            items.removeAt(position)
        }
        adapter.callback = { item, position ->
            items.add(item)

        }
        adapter.setData(items)


    }

    private fun setData() {
        items.add(Item(mipmap.ic_launcher))
        items.add(Item(mipmap.ic_launcher))
        items.add(Item(mipmap.ic_launcher))
        items.add(Item(mipmap.ic_launcher, Item.Person("Jemali", "Kakauridze")))
        items.add(Item(mipmap.ic_launcher, Item.Person("Jemali", "Kakauridze")))
        items.add(Item(mipmap.ic_launcher, Item.Person("Jemali", "Kakauridze")))
        items.add(Item(mipmap.ic_launcher))
        items.add(Item(mipmap.ic_launcher))
        items.add(Item(mipmap.ic_launcher))
        items.add(Item(mipmap.ic_launcher, Item.Person("Jemali", "Kakauridze")))
        items.add(Item(mipmap.ic_launcher, Item.Person("Jemali", "Kakauridze")))
        items.add(Item(mipmap.ic_launcher, Item.Person("Jemali", "Kakauridze")))
        items.add(Item(mipmap.ic_launcher))
        items.add(Item(mipmap.ic_launcher, Item.Person("Jemali", "Kakauridze")))
        items.add(Item(mipmap.ic_launcher, Item.Person("Jemali", "Kakauridze")))
        items.add(Item(mipmap.ic_launcher))
        items.add(Item(mipmap.ic_launcher, Item.Person("Jemali", "Kakauridze")))
        items.add(Item(mipmap.ic_launcher, Item.Person("Jemali", "Kakauridze")))
        items.add(Item(mipmap.ic_launcher))
        items.add(Item(mipmap.ic_launcher))
        items.add(Item(mipmap.ic_launcher, Item.Person("Jemali", "Kakauridze")))

    }
}